# Jitsicfg

Tool to configure Jitsi server from shell scripts (or command line). 

## Installation

```
sudo cp ./jitsicfg /usr/local/bin
sudo chmod +x /usr/local/bin/jitsicfg
```
## Usage

```
jitsicfg is a tool to configure Jitsi servers.

It provides functions to modify configuration files from shell scripts.
Usage: jitsicfg COMMAND CFG_FILE ARG [ARG ...]

Available commands are:

jitsi::enablesection
jitsi::enablesectionsetting
jitsi::enablesetting
jitsi::getsectionsetting
jitsi::getsetting
jitsi::setsectionsetting
jitsi::setsetting
logrotate::rmfield
logrotate::setfield
prosody::addvhost
prosody::vhost_additem2section
prosody::vhost_setfield

To know the syntax for each command, type: jitsicfg COMMAND --help

```
## Commands

```
  Command: jitsi::enablesection CFG_FILE SECTION
  
  Enable (uncomment) SECTION (and its children sections, if any) in CFG_FILE
  It works also when key:value pairs are on separate (commented) lines; "real"
  comments are kept and not uncommented.
  
  Example:
  jitsicfg jitsi::enablesection \
    /etc/jitsi/meet/meet.example.com-config.js dropbox

```


```
  Command: jitsi::enablesectionsetting CFG_FILE SECTION SETTING
  
  Enable (uncomment) SETTING within SECTION.d
  
  Example:
   jitsi::enablesectionsetting \
    /etc/jitsi/meet/meet.example.com-config.js p2p preferH264

```


```
  Command: jitsi::enablesetting CFG_FILE SETTING
  
  Enable (uncomment) SETTING
  
  Example:
  jitsicfg jitsi::enablesetting \
    /etc/jitsi/meet/meet.example.com-config.js startAudioMuted

```


```
  Syntax: jitsi::getsectionsetting SECTION SETTING
  
  Return value of SETTING within SECTION. If value is quoted, it will be
  returned quoted. If SETTING line is commented out, return empty string.
  
  Example:
   jitsi::getsectionsetting \
    /etc/jitsi/meet/meet.example.com-config.js constraints max

```


```
  Syntax: jitsi::getsetting SETTING

  Return SETTING's value. If value is quoted, it will be returned quoted.
  If SETTING line is commented out, return empty string.
  
  Example:
   jitsi::getsetting \
     /etc/jitsi/meet/meet.example.com-config.js bosh

```


```
  Command: jitsi::setsectionsetting SECTION SETTING VALUE
  
  Set SETTING to VALUE within SECTION.  If VALUE needs quotes, you have to pass
  them in the argument (e.g. '"test"').
  
  Example:
  jitsicfg jitsi::setsectionsetting \
    /etc/jitsi/meet/meet.example.com-config.js constraints max 480

```


```
  Command: jitsi::setsetting SETTING VALUE
  
  Set SETTING to VALUE.  If VALUE needs quotes, you have to pass them in the 
  argument (e.g. '"test"').
  
  Example:
   jitsi::setsetting \
    /etc/jitsi/meet/meet.example.com-config.js startAudioMuted 5

```


```
  Command: logrotate::rmfield CFG_FILE LOGFILE FIELD
  
  Remove FIELD in LOGFILE section of CFG_FILE
  
  Example:
  jitsicfg logrotate::rmfield \
    /etc/logrotate.d/jitsi-videobridge \
    /var/log/jitsi/jvb.log size

```


```
  Command: logrotate::setfield CFG_FILE LOGFILE FIELD [VALUE]
  
  Set FIELD to VALUE in LOGFILE section of CFG_FILE; if FIELD does not exist,
  add it.
  
  Example:
  jitsicfg logrotate::setfield \
    /etc/logrotate.d/jitsi-videobridge \
    /var/log/jitsi/jvb.log maxage 7

```


```
  Command: prosody::addvhost CFG_FILE VHOST_FQDN
  
  Add a VirtualHost identified by VHOST_FQDN to CFG_FILE, if it does not exist.
  
  Example: 
  jitsicfg prosody::addvhost \
    /etc/prosody/conf.avail/meet.example.com.cfg.lua \
    guest.meet.example.com

```


```
  Command: prosody::vhost_additem2section CFG_FILE VHOST_FQDN SECTION ITEM
  
  Add ITEM to the specified SECTION of the VirtualHost identified by
  VHOST_FQDN in CFG_FILE; if SECTION does not exist, create it and add ITEM;
  if ITEM already exist, do nothing; in any case, ignore possible lua comments
  starting with '--'. If VALUE needs quotes, you have to pass them in the 
  argument (e.g. '"test"').
  
  Example:
  jitsicfg prosody::vhost_additem2section \
    /etc/prosody/conf.avail/meet.example.com.cfg.lua \
    meet.example.com \
    modules_enabled '"log_auth"'

```


```
  Command: prosody::vhost_setfield CFG_FILE VHOST_FQDN FIELD VALUE
  
  Set FIELD to VALUE for VirtualHost identified by VHOST_FQDN in CFG_FILE; if
  FIELD does not exist, add it. If FIELD needs quotes, you have to pass them in
  the argument (e.g. '"test"')
  
  Example: 
  jitsicfg prosody::vhost_setfield \
    /etc/prosody/conf.avail/meet.example.com.cfg.lua \
    guest.meet.example.com \
    authentication '"anonymous"'

```


