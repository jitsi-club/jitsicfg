#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-only
# SPDX-FileCopyrightText: 2020 Alberto Pianon <pianon@array.eu>

# NOTE: using :: in function names for clearer code, even if it is not POSIX
# compliant (anyway, it is supported in bash)

# NOTE: Comments to insanely long Perl oneliners require an editor without soft
# wrap to be readable

function _init_vars {
  jitsicfg=$(basename $0)
  functions="$(declare -F)"
  functions="${functions//declare -f /}"
  functions="${functions/_init_vars/}"
  functions="${functions/_check_help/}"
  functions="${functions/_check_root/}"
  functions="${functions/_main_help/}"
  functions="${functions/_list_functions/}"
  functions="${functions/_command_exists/}"
  functions=$(echo "$functions" | sed -r '/^\s*$/d')
  IFS=$'\n'; func_array=($functions); unset IFS
}

function _check_root {
  if [[ $EUID != 0 ]]; then
    echo "This command must be run as root."
    exit 1
  fi
}

function _check_help {
  if [[ -z $1 || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "$HELP_MSG"
    exit 0
  fi
}

function _list_functions {
  if [[ "$1" == "--list-functions"  ]]; then
    echo "$functions"
    exit 0
  fi
}

function _main_help { HELP_MSG="\

$jitsicfg is a tool to configure Jitsi servers.

It provides functions to modify configuration files from shell scripts.
Usage: $jitsicfg COMMAND CFG_FILE ARG [ARG ...]

Available commands are:

$functions

To know the syntax for each command, type: $jitsicfg COMMAND --help
"  
  _check_help $1
}

function _command_exists {
  if [[ ! "${func_array[@]}" =~ "${1}" ]]; then
    echo "ERROR: command $1 does not exist."
    exit 1
  fi
}

function prosody::addvhost { HELP_MSG="\

  Command: prosody::addvhost CFG_FILE VHOST_FQDN
  
  Add a VirtualHost identified by VHOST_FQDN to CFG_FILE, if it does not exist.
  
  Example: 
  $jitsicfg prosody::addvhost \\
    /etc/prosody/conf.avail/meet.example.com.cfg.lua \\
    guest.meet.example.com
"
    
  _check_help $1
  PERL_ADDVHOST='BEGIN { $/ = undef; $\ = undef; } if (/(^VirtualHost\s+\"$FQDN"(\n([ ]|\t|--|^$)+.*)*)/m) { print; } else { print; print("\nVirtualHost \"$FQDN\"\n".(" "x4)."-- config for vhost $FQDN\n\n"); }'
  #              └──────────────┬─────────────────┘      └──────────┬──────────┘ └─────────┬──────────┘     └───────────────────────────────────────────────────┬────────────────────────────────────────────┘                              
  #                     multiline processing       search for Virtualhost at the  ...followed by a series                  if the configuration section is found, print the text as is (i.e. do nothing);
  #                                                beginning of a line followed   of tab/space indented                    otherwise, add it at the end ("header" and a 4-space indented line (comment))
  #                                                by at least 1 space and by     lines or commented lines
  #                                                FQDN between quotes...        
  #                                                └────────────────────────────┬────────────────────────┘
  #                                                search for the configuration section of Virtualhost "FQDN"
  perl -sne "$PERL_ADDVHOST" -i -- -FQDN=$2 $1
}

function prosody::vhost_setfield { HELP_MSG="\

  Command: prosody::vhost_setfield CFG_FILE VHOST_FQDN FIELD VALUE
  
  Set FIELD to VALUE for VirtualHost identified by VHOST_FQDN in CFG_FILE; if
  FIELD does not exist, add it. If FIELD needs quotes, you have to pass them in
  the argument (e.g. '\"test\"')
  
  Example: 
  $jitsicfg prosody::vhost_setfield \\
    /etc/prosody/conf.avail/meet.example.com.cfg.lua \\
    guest.meet.example.com \\
    authentication '\"anonymous\"'
"
    
  _check_help $1
  PERL_VH_SETFIELD='BEGIN { $/ = undef; $\ = undef; } s%(^VirtualHost\s+\"$FQDN\"(\n([ ]|\t|--|^$)+.*)*)% my $x = $1; if ($x =~ m#^\s+$field#m) { $x =~ s#^(\s+$field\s*=\s*).*#$1.$value#me; } else { $x = $x.(" "x4).$field." = ".$value."\n"; } $x %me;'
  #               └──────────────┬─────────────────┘      └──────────┬──────────┘ └─────────┬──────────┘  └─────┬───┘    └─────────┬──────────┘ └──────────────────────┬──────────────────────┘      └──────────────────────┬───────────────────┘  └┬┘                                
  #                      multiline processing       search for Virtualhost at the  ...followed by a series      │          search for $field           if $field is found, replace its                  ...otherwise, add an indented line with     │
  #                                                 beginning of a line followed   of tab/space indented        │         at the beginning of                value with $value...                       $fileld = $value                            │
  #                                                 by at least 1 space and by     lines or commented lines     │           an indented line                                                                                                        │
  #                                                 FQDN between quotes...                                      │                                                                                                           print the modified configuration section
  #                                                 └────────────────────────────┬────────────────────────┘     │                                                                                                                            as replacement
  #                                               search for the configuration section of Virtualhost "FQDN"    └─ copy the string of the found configuration section to $x, because $1 is not modifiable

  perl -spe "$PERL_VH_SETFIELD" -i -- -FQDN=$2 -field=$3 -value=$4 $1
}

function prosody::vhost_additem2section { HELP_MSG="\

  Command: prosody::vhost_additem2section CFG_FILE VHOST_FQDN SECTION ITEM
  
  Add ITEM to the specified SECTION of the VirtualHost identified by
  VHOST_FQDN in CFG_FILE; if SECTION does not exist, create it and add ITEM;
  if ITEM already exist, do nothing; in any case, ignore possible lua comments
  starting with '--'. If VALUE needs quotes, you have to pass them in the 
  argument (e.g. '\"test\"').
  
  Example:
  $jitsicfg prosody::vhost_additem2section \\
    /etc/prosody/conf.avail/meet.example.com.cfg.lua \\
    meet.example.com \\
    modules_enabled '\"log_auth\"'
"

  _check_help $1    
  PERL_ADDITEM='BEGIN { $/ = undef; $\ = undef; } s%(^VirtualHost\s+\"$FQDN"(\n([ ]|\t|--|^$)+.*)*)% my $x = $1; if ($x =~ m#^\s+$sec#m) { $x =~ s#(^\s+$sec\s*=\s*)\{([^}]+)\}# my $z = $1; my $y = $2; $y = ($y =~ m&^\s+$item&m) ? $y : $y.(" "x4)."$item;\n"." "x8; $z."{".$y."}" #me; } else { $x = $x.(" "x8)."$sec = {\n".(" "x12)."$item;\n".(" "x8)."}";} $x %me;'
  #            └──────────────┬─────────────────┘    └──────────┬──────────┘ └─────────┬──────────┘  └─────┬───┘    └────────┬─────────┘ └─────────────┬──────────────────────┘  └──┬──────────────────┘  └──────────────────────────────┬────────────────────────────┘ └──────┬────┘        └─────────────────────────────────────┬─────────────────────────────┘ └┬┘                       
  #                   multiline processing     search for Virtualhost at the  ...followed by a series      │          search for $sec      if found, search for the content         │                    if, within content between curly brackets, $item is found at the      │                                    else, if $sec does *not* exist,                 │
  #                                            beginning of a line followed   of tab/space indented        │         at the beginning of      between curly brackets                │                  beginning of an uncommented indented line, keep it, otherwise add it    │                                    create it, with $item inside curly brackets     │
  #                                            by at least 1 space and by     lines or commented lines     │          an indented line                                              │                                                                                          │                                    after it                                        │
  #                                            FQDN between quotes...                                      │                                       copy the string of the found section and its content to $z and $y, because $1 and $2 are not modifiable                     │                                                                       print the modified section
  #                                            └────────────────────────────┬────────────────────────┘     │                                                                                                                                                                   │                                                                             as replacement
  #                                          search for the configuration section of Virtualhost "FQDN"    └─ copy the string of the found configuration section to $x, because $1 is not modifiable                                              print the modified section (it goes into $x, since we are into a replacement expression)

  perl -spe "$PERL_ADDITEM" -i -- -FQDN=$2 -sec=$3 -item=$4 $1
}

function logrotate::setfield { HELP_MSG="\

  Command: logrotate::setfield CFG_FILE LOGFILE FIELD [VALUE]
  
  Set FIELD to VALUE in LOGFILE section of CFG_FILE; if FIELD does not exist,
  add it.
  
  Example:
  $jitsicfg logrotate::setfield \\
    /etc/logrotate.d/jitsi-videobridge \\
    /var/log/jitsi/jvb.log maxage 7
"

  _check_help $1
  PERL_SETFIELD='BEGIN { $/ = undef; $\ = undef; } s%^$logfile([^{]+)\{([^}]+)\}% my $y = $1; my $x = $2; if ($x =~ m#^\s*$field#m) { $x =~ s#(^\s*$field).*$#$1." ".$value#me; } else { $x = $x.(" "x4).$field." ".$value."\n"; } $logfile.$y."{".$x."}" %mse;'
  #            └──────────────┬─────────────────┘    └────────────┬────────────┘  └───┬─────────────────┘ └────────────┬──────────┘  └──────────────────┬─────────────────────┘   └──────────────────────┬───────────────────────┘ └───────┬────────────┘
  #                   multiline processing                        │                   │                      if $field is found           ...replace it with '$field $value'            ...otherwise, add a 4-space indented       print the modified logfile section
  #                                              search for $logfile (followed by     │                      within fields...          (BTW, if $value is empty this part does                 line '$field $value'                as replacement
  #                                              other possible logfile names)        │                                                       not modify anything),...
  #                                              followed by fields within curly      │
  #                                              brackets                             │
  #                                                                                   └─ copy the found logfile section ($1) and its fields ($2) to $y and $x, because $1 and $2 are not modifiable
  
  perl -spe "$PERL_SETFIELD" -i -- -logfile=$2 -field=$3 -value=$4 $1
}

function logrotate::rmfield { HELP_MSG="\

  Command: logrotate::rmfield CFG_FILE LOGFILE FIELD
  
  Remove FIELD in LOGFILE section of CFG_FILE
  
  Example:
  $jitsicfg logrotate::rmfield \\
    /etc/logrotate.d/jitsi-videobridge \\
    /var/log/jitsi/jvb.log size
"
  
  _check_help $1  
  PERL_RMFIELD='BEGIN { $/ = undef; $\ = undef; } s%^$logfile([^{]+)\{([^}]+)\}% my $y = $1; my $x = $2; $x =~ s#(^\s*$field.*$)##m; $logfile.$y."{".$x."}" %me;'
  #           └──────────────┬─────────────────┘    └────────────┬────────────┘  └───┬─────────────────┘ └───────┬─────────────────┘ └───────┬────────────┘ 
  #                  multiline processing                        │                   │                    remove $field line if       print the modified logfile section
  #                                             search for $logfile (followed by     │                       found                    as replacement
  #                                             other possible logfile names)        │
  #                                             followed by fields within curly      │
  #                                             brackets                             │
  #                                                                                  └─ copy the found logfile section ($1) and its fields ($2) to $y and $x, because $1 and $2 are not modifiable
  perl -spe "$PERL_RMFIELD" -i -- -logfile=$2 -field=$3 $1
}

function jitsi::enablesection { HELP_MSG="\

  Command: jitsi::enablesection CFG_FILE SECTION
  
  Enable (uncomment) SECTION (and its children sections, if any) in CFG_FILE
  It works also when key:value pairs are on separate (commented) lines; \"real\"
  comments are kept and not uncommented.
  
  Example:
  $jitsicfg jitsi::enablesection \\
    /etc/jitsi/meet/meet.example.com-config.js dropbox
"
    
  _check_help $1
  PERL_ENABLE_SEC='BEGIN { $/ = undef; $\ = undef; } s%(^[\s\/]*$sec\:\s*)(\{(?:[^{}]++|(?2))*\})% my $x = $1.$2; $x =~ s#(^[\s\/]*\w+\:\s+\[[^\]]+\]|^[\s\/]*\w+\:\s[\s(?:\/\/.*)]*[^\s\/]+|^\s*\/\/\s*\})# my $y = $1; $y =~ s&^([ \t]*)\/\/\s?&$1&mg;                              $y #mseg; $x =~ s#\/\/(\s+\/\/)#$1#g; $x %mxe'
  #               └──────────────┬─────────────────┘ └──────────────────┬──────────────────────┘ └─────────┬────┘         └────────────┬────────────┘ └─────────────┬──────────────────────┘ └────────┬───┘  └────────┬┘ └─────────────┬───────────────┘                              └┬┘       └────────────┬───────────┘  └┬┘
  #                    multiline processing      search for $sec at the beginning of an indented/commented │              search for (a) lists, like   ...or (b) 'setting: value'          ...or (c)                  │  globally replace '(possible indentation)//(possible space)'  print      remove possible double //   │
  #                                              line followed by ':' by its settings (and possible        │                  'list_name: [ stuff ]'   pairs, even if 'value' is on        commented closing          │  with '(possible indentation)' to uncomment line              into $x    like in '  //   // comment' └─print modified stuff as replacement
  #                                              subsections) within curly brackets; since subsections     │                          ...              a different line and there are      curly brackets             │                                                               
  #                                              have curly brackets, too, we use perl regex recursive     │                                           comments in the middle...                                      └─copy found stuff in $y because $1 is not modifiable        
  #                                              patterns to match balanced text                           │                                                                                                                                                            
  #                                              (see https://www.perlmonks.org/?node_id=660316)           └─ copy the found section ($1 is '(possible indentation)$sec:', $2 is the content within curly brackets) because $1 and $2 are not modifiable               
  perl -spe "$PERL_ENABLE_SEC" -i -- -sec=$2 $1
}

function jitsi::enablesetting { HELP_MSG="\

  Command: jitsi::enablesetting CFG_FILE SETTING
  
  Enable (uncomment) SETTING
  
  Example:
  $jitsicfg jitsi::enablesetting \\
    /etc/jitsi/meet/meet.example.com-config.js startAudioMuted
"
    
  _check_help $1    
  PERL_ENABLE_SET='BEGIN { $/ = undef; $\ = undef; } s%(^[\s\/]*$setting\:[\s\/]*[^\s\/]+)% my $x = $1; $x =~ s!^([ \t]*)\/\/\s?!$1!mg;                           $x %me'
  #               └──────────────┬─────────────────┘    └──────────────┬─────────────────┘ └─────┬───┘  └──────────────┬──────────────┘                            └┬┘ 
  #                    multiline processing            search for '$setting: value' pair,        │   globally replace '(possible indentation)//(possible space)'    └─print modified (uncommented) '$setting: value' pair as replacement
  #                                                    line and there are comments in the        │
  #                                                    middle                                    │
  #                                                                                              │
  #                                                                                              │
  #                                                                                              └─ copy the found '$setting: value' pair into $x, since $1 is not modifiable
  perl -spe "$PERL_ENABLE_SET" -i -- -setting=$2 $1  
}

function jitsi::enablesectionsetting { HELP_MSG="\

  Command: jitsi::enablesectionsetting CFG_FILE SECTION SETTING
  
  Enable (uncomment) SETTING within SECTION.d
  
  Example:
  $me jitsi::enablesectionsetting \\
    /etc/jitsi/meet/meet.example.com-config.js p2p preferH264
"
    
  _check_help $1  
  PERL_ENABLE_SECSET='BEGIN { $/ = undef; $\ = undef; } s%(^[\s\/]*$sec\:\s*)(\{(?:[^{}]++|(?2))*\})% my $x = $1.$2; $x =~ s#(^[\s\/]*$setting\:[\s\/]*[^\s\/]+)# my $y = $1; $y =~ s!^([ \t]*)\/\/\s?!$1!mg; $y #me; $x %me'
  #                  └──────────────┬─────────────────┘ └──────────────────┬──────────────────────┘ └─────────┬────┘         └──────────────┬─────────────────┘   └─────┬───┘ └──────────────┬──────────────┘ └──────┬──┘ 
  #                       multiline processing      search for $sec at the beginning of an indented/commented │              search for '$setting: value' pair,         │           globally replace                 └─────── print modified (uncommented) line as replacement              
  #                                                 line followed by ':' by its settings (and possible        │              even if 'value' is on a different          │     '(possible indentation)//(possible space)'
  #                                                 subsections) within curly brackets; since subsections     │              line and there are comments in the         │     with '(possible indentation)' to uncomment line
  #                                                 have curly brackets, too, we use perl regex recursive     │              middle                                     │
  #                                                 patterns to match balanced text                           │                                                         └copy the found '$setting: value' pair into $x, since $1 is not modifiable
  #                                                 (see https://www.perlmonks.org/?node_id=660316)           │
  #                                                                                                           └─ copy the found section ($1 is '(possible indentation)$sec:', $2 is the content within curly brackets)
  perl -spe "$PERL_ENABLE_SECSET" -i -- -sec=$2 -setting=$3 $1
}

function jitsi::setsetting { HELP_MSG="\

  Command: jitsi::setsetting SETTING VALUE
  
  Set SETTING to VALUE.  If VALUE needs quotes, you have to pass them in the 
  argument (e.g. '\"test\"').
  
  Example:
  $me jitsi::setsetting \\
    /etc/jitsi/meet/meet.example.com-config.js startAudioMuted 5
"
    
  _check_help $1  
  PERL_SETSETTING='BEGIN { $/ = undef; $\ = undef; } s%(^[ \t]*$setting\:\s*)(\S+)%$1$value\,%m'
  #               └──────────────┬─────────────────┘ └───────────────────┬────────────────────┘
  #                      multiline processing        replace '(possible indentation)$setting: whatever'
  #                                                  with  '(possible indentation)$setting: $value'
  perl -spe "$PERL_SETSETTING" -i -- -setting=$2 -value=$3 $1
}

function jitsi::getsetting { HELP_MSG="\

  Syntax: jitsi::getsetting SETTING

  Return SETTING's value. If value is quoted, it will be returned quoted.
  If SETTING line is commented out, return empty string.
  
  Example:
  $me jitsi::getsetting \\
     /etc/jitsi/meet/meet.example.com-config.js bosh
"
  _check_help $1
  PERL_GETSETTING='BEGIN { $/ = undef; $\ = undef; } /(^[ \t(?:\/\/)]*$setting\:\s*)([^ \n\t,]+)*/m; if ($1 && $2 && $1 !~ /^[ \t]*\/\//m) {print "$2";}'
  #               └──────────────┬─────────────────┘  └─────────────────────┬──────────────────────┘ └─────────────────────────┬───────────────────────┘
  #                        multiline processing                            find                                    if found and line is not commented,
  #                                              '(possible indentation and/or comment tag)$setting: whatever'              print 'whatever' (i.e. value)
  perl -sne "$PERL_GETSETTING" -- -setting=$2 $1
}

function jitsi::getsectionsetting { HELP_MSG="\

  Syntax: jitsi::getsectionsetting SECTION SETTING
  
  Return value of SETTING within SECTION. If value is quoted, it will be
  returned quoted. If SETTING line is commented out, return empty string.
  
  Example:
  $me jitsi::getsectionsetting \\
    /etc/jitsi/meet/meet.example.com-config.js constraints max
"
  
  _check_help $1
  PERL_GETSECTIONSETTING='BEGIN { $/ = undef; $\ = undef; } /(^[\s\/]*$sec\:\s*)(\{(?:[^{}]++|(?2))*\})/m; my $x = $2; $x =~ /(^[ \t(?:\/\/)]*$setting\:\s*)([^ \n\t,]+)*/m; if ($1 && $2 && $1 !~ /^[ \t]*\/\//m) {print "$2";}'
  #                      └──────────────┬─────────────────┘  └────────────────────┬─────────────────────┘ └────────┬─┘  └────────────────────────┬────────────────────────┘  └─────────────────────────┬───────────────────────┘
  #                             multiline processing       search for $sec at the beginning of indented/commented  │                           find                                      if found and line is not commented,
  #                                                        line followed by ':' by its settings (and possible      │   '(possible indentation and/or comment tag)$setting: whatever'         print 'whatever' (i.e. value)              
  #                                                        subsections) within curly brackets; since subsections   │
  #                                                        have curly brackets, too, we use perl regex recursive   │
  #                                                        patterns to match balanced text                         └─ copy the found section's content (within curly brackets) to $x
  #                                                        (see https://www.perlmonks.org/?node_id=660316)            for further processing
  perl -sne "$PERL_GETSECTIONSETTING" -- -sec=$2 -setting=$3 $1
}


function jitsi::setsectionsetting { HELP_MSG="\

  Command: jitsi::setsectionsetting SECTION SETTING VALUE
  
  Set SETTING to VALUE within SECTION.  If VALUE needs quotes, you have to pass
  them in the argument (e.g. '\"test\"').
  
  Example:
  $jitsicfg jitsi::setsectionsetting \\
    /etc/jitsi/meet/meet.example.com-config.js constraints max 480
"
  
  _check_help $1
  PERL_SETSECTIONSETTING='BEGIN { $/ = undef; $\ = undef; } s%(^[\s\/]*$sec\:\s*)(\{(?:[^{}]++|(?2))*\})% my $x = $1.$2; $x =~ s#(^[ \t]*$setting\:\s*)(\S+)#$1$value\,#mg; $x %me'
  #                      └──────────────┬─────────────────┘ └──────────────────┬──────────────────────┘ └─────────┬────┘         └───────────────────┬────────────────────┘ └┬┘ 
  #                           multiline processing      search for $sec at the beginning of an indented/commented │      replace '(possible indentation)$setting: whatever'  │
  #                                                     line followed by ':' by its settings (and possible        │      with  '(possible indentation)$setting: $'           │
  #                                                     subsections) within curly brackets; since subsections     │                                                          └─print modified '$setting: $value' pair as replacement
  #                                                     have curly brackets, too, we use perl regex recursive     │
  #                                                     patterns to match balanced text                           └─ copy the found section ($1 is '(possible indentation)$sec:', $2 is the content within curly brackets)
  #                                                     (see https://www.perlmonks.org/?node_id=660316)                                       
  perl -spe "$PERL_SETSECTIONSETTING" -i -- -sec=$2 -setting=$3 -value=$4 $1 
}

# TODO: jitsi::enablelist, jitsi::add2list, jitsi::removefromlist

_check_root
_init_vars
_main_help $1
_list_functions $1
_command_exists $1
$1 $2 $3 $4 $5
